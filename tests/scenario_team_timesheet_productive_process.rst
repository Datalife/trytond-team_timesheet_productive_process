====
Team Timesheet productive process manage
====

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team
    >>> from proteus import Model, Wizard


Install team_timesheet_productive_process Module::

    >>> config = activate_modules('team_timesheet_productive_process')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get today date::

    >>> import datetime
    >>> today = datetime.date.today()

Create team::

    >>> _ = create_team()
    >>> team = get_team()


Create a team timesheet without productive process::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.productive_process == None
    True


Insert first productive process::
    >>> Process = Model.get('productive.process')
    >>> process1 = Process(name='Process 1')
    >>> process1.save()


Create a team timesheet with one productive process::

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.productive_process.name
    'Process 1'
    >>> tts.save()
    >>> tts = TeamTimesheet(tts.id)
    >>> tts.productive_process.name
    'Process 1'


Insert second productive process::

    >>> Process = Model.get('productive.process')
    >>> process2 = Process(name='Process 2')
    >>> process2.save()


Create a team timesheet with two productive process::

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.productive_process == None
    True

Set productive process 2 as default::

    >>> config._context['productive_process'] = process2.id


Create a team timesheet with context default productive process::

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.productive_process.name
    'Process 2'
    >>> tts.save()
    >>> tts = TeamTimesheet(tts.id)
    >>> tts.productive_process.name
    'Process 2'