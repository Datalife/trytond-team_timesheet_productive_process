# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.productive_process import ProductiveProcDocMixin


class TeamTimesheet(ProductiveProcDocMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'


class TeamTimesheetWork(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    productive_process = fields.Function(fields.Many2One(
        'productive.process', 'Productive process'),
        'on_change_with_productive_process')

    @fields.depends('team_timesheet',
        '_parent_team_timesheet.productive_process')
    def on_change_with_productive_process(self, name=None):
        return (self.team_timesheet
            and self.team_timesheet.productive_process
            and self.team_timesheet.productive_process.id)
